//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/

	{"🎸 ", "~/.local/bin/music",			        1,		                1},

	{"💽 ", "~/.local/bin/memory",				1,				2},

	{"🔊 ", "~/.local/bin/volume",				30,				3},

	{"🔋 ", "~/.local/bin/battery",				30,				4},

	{"💡 ", "~/.local/bin/brightness",			30,				5},

	{"🕑 ", "date '+%b %d (%a) %I:%M%p'",			60,				6},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
